package se331.lab.rest.dao;


import com.fasterxml.jackson.annotation.JacksonAnnotation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Profile("MyDao")
@Repository
public class StudentDaDaoDaoImpl implements StudentDao {
    List<Student> students;

    public StudentDaDaoDaoImpl() {
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(4l)
                .studentId("SE-004")
                .name("Post")
                .surname("Malone")
                .gpa(1.92)
                .image("https://specials-images.forbesimg.com/imageserve/626737584/960x0.jpg?fit=scale")
                .penAmount(0)
                .description("Go Flex!")
                .build());
        this.students.add(Student.builder()
                .id(5l)
                .studentId("SE-005")
                .name("Joji")
                .surname("Sushitrash")
                .gpa(2.34)
                .image("https://pbs.twimg.com/profile_images/1162428593047162880/6dvIbVoQ_400x400.jpg")
                .penAmount(0)
                .description("Welcome to sanctuary")
                .build());
        this.students.add(Student.builder()
                .id(6l)
                .studentId("SE-006")
                .name("Cuco")
                .surname("Icryduringsex")
                .gpa(1.77)
                .image("https://bloximages.newyork1.vip.townnews.com/postguam.com/content/tncms/assets/v3/editorial/b/06/b0699d4c-a124-11e9-8e01-5771c6fd90a0/5d22a4a039957.image.jpg?resize=1200%2C800")
                .penAmount(0)
                .description("Bossa No Sè")
                .build());

    }

    @Override
    public List<Student> getAllStudent() {
        log.info("My dao is called");
        return students;
    }

    @Override
    public Student findById(Long id) {
        return students.get((int) (id - 1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) students.size());
        students.add(student);
        return student;
    }
}
